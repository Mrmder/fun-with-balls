﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hazardMovementLevel1Only : MonoBehaviour
{
    private double hazardMin;
    private double hazardMax;
    bool xPosition;
    public float speed = 0f;

    private void Start()
    {
        hazardMin = transform.position.x - 7;
        hazardMax = transform.position.x + 7;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (transform.position.x <= hazardMin)
            xPosition = true;
        else if (transform.position.x >= hazardMax)
            xPosition = false;

        if (xPosition)
        {
            transform.position += new Vector3(0.1f + speed, 0, 0);
        }
        else if (!xPosition)
        {
            transform.position += new Vector3(-0.1f - speed, 0, 0);
        }
    }
}