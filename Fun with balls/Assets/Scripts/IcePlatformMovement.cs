﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IcePlatformMovement : MonoBehaviour
{
    private double hazardMinHeight;
    private double hazardMaxHeight;
    bool height;
    public float speed = 0f;

    private void Start()
    {
        hazardMinHeight = transform.position.y;
        hazardMaxHeight = transform.position.y + 50;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (transform.position.y <= hazardMinHeight)
            height = true;
        else if (transform.position.y >= hazardMaxHeight)
            height = false;

        if (height)
        {
            transform.position += new Vector3(0, 0.08f + speed, 0);
        }
        else if (!height)
        {
            transform.position += new Vector3(0, -0.08f - speed, 0);
        }
    }
}