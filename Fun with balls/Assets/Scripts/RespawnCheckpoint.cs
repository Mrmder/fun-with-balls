﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnCheckpoint : MonoBehaviour
{    
    private Transform SpawnPoint;    
    private GameObject player;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            player.transform.position = SpawnPoint.position;
        }
    }
}
