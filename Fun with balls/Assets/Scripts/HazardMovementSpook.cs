﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HazardMovementSpook : MonoBehaviour
{
    private double hazardMinX;
    private double hazardMaxX;
    private double hazardMinZ;
    private double hazardMaxZ;
    bool xPosition = true;
    bool zPosition = false;
    public float speed = 0f;
    public float area = 3;

    private void Start()
    {
        hazardMinX = transform.position.x - area;
        hazardMaxX = transform.position.x + area;
        hazardMinZ = transform.position.z - area;
        hazardMaxZ = transform.position.z + area;

    }

    private void One()
    {
        transform.position += new Vector3(0.1f + speed, 0, 0);
    }

    private void Two()
    {
        transform.position += new Vector3(0, 0, 0.1f + speed);
    }

    private void Three()
    {
        transform.position += new Vector3(-0.1f - speed, 0, 0);
    }

    private void Four()
    {
        transform.position += new Vector3(0, 0, -0.1f - speed);
    }

    void FixedUpdate()
    {
        if (transform.position.x <= hazardMinX)
            xPosition = true;
        if (transform.position.x >= hazardMaxX)
            xPosition = false;
        if (transform.position.z <= hazardMinZ)
            zPosition = false;
        if (transform.position.z >= hazardMaxZ)
            zPosition = true;



            if (xPosition && !zPosition)
                One();
            else if (!xPosition && !zPosition)
                Two();
            else if (!xPosition && zPosition)
                Three();
            else if (xPosition && zPosition)
                Four();




    }
}
