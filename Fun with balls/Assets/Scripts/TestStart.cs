﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class TestStart : MonoBehaviour
{
    public Button[] levelButtons;
   

    private void Start()
    {
        int levelReached = PlayerPrefs.GetInt("levelReached", 1);

        for (int i = 0; i < levelButtons.Length; i++)
        {
            if (i + 1 > levelReached)
            {
            levelButtons[i].interactable = false;
            }
        }          
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.U))
        {
            levelButtons[1].interactable = true;
            levelButtons[2].interactable = true;
            //levelButtons[3].interactable = true;
            //levelButtons[4].interactable = true;
            //levelButtons[5].interactable = true;
        }
        
    }   

    public void Level_1()
    {
        SceneManager.LoadScene(1);
    }

    public void Level_2()
    {
        SceneManager.LoadScene(2);
    }

    public void Level_3()
    {
        SceneManager.LoadScene(3);
    }

    public void Level_4()
    {
        SceneManager.LoadScene(4);
    }

    public void Level_5()
    {
        SceneManager.LoadScene(5);
    }

    public void Level_6()
    {
        SceneManager.LoadScene(6);
    }

    public void quit()
    {
        Application.Quit();
        Debug.Log("the user has quit the game");
    }
}

   
       
      











