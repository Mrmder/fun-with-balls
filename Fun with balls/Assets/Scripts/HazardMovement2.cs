﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HazardMovement2 : MonoBehaviour
{
    private double hazardMinSize;
    private double hazardMaxSize;
    bool scale;
    public float speed = 0f;

    private void Start()
    {
        hazardMinSize = 1;
        hazardMaxSize = 10;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (transform.localScale.x < hazardMinSize)
            scale = true;
        else if (transform.localScale.x > hazardMaxSize)
            scale = false;

        if (scale)
        {
                transform.localScale += new Vector3(0.1F + speed, 0, 0);
        }
        if (!scale)
        {
            transform.localScale += new Vector3(-0.1F - speed, 0, 0);
        }
    }
}