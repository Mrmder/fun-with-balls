﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HazardMovementSpook3 : MonoBehaviour
{
    private double hazardMinZ;
    private double hazardMaxZ;
    private double hazardMinY;
    private double hazardMaxY;
    bool zPosition = true;
    bool yPosition = false;
    public float speed = 0f;
    public float area = 3;

    private void Start()
    {
        hazardMinZ = transform.position.z - area;
        hazardMaxZ = transform.position.z + area;
        hazardMinY = transform.position.y - area;
        hazardMaxY = transform.position.y + area;

    }

    private void One()
    {
        transform.position += new Vector3(0, 0, 0.1f + speed);
    }

    private void Two()
    {
        transform.position += new Vector3(0, 0.1f + speed, 0);
    }

    private void Three()
    {
        transform.position += new Vector3(0, 0, -0.1f - speed);
    }

    private void Four()
    {
        transform.position += new Vector3(0, -0.1f - speed, 0);
    }

    void FixedUpdate()
    {
        if (transform.position.z <= hazardMinZ)
            zPosition = true;
        if (transform.position.z >= hazardMaxZ)
            zPosition = false;
        if (transform.position.y <= hazardMinY)
            yPosition = false;
        if (transform.position.y >= hazardMaxY)
            yPosition = true;



        if (zPosition && !yPosition)
            One();
        else if (!zPosition && !yPosition)
            Two();
        else if (!zPosition && yPosition)
            Three();
        else if (zPosition && yPosition)
            Four();




    }
}
