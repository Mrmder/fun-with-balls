﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ObstacleCollision : MonoBehaviour
{
    CheckPoint _respawn;
    CheckPoint _spawnPoint;    

    private GameObject player;    

    private void Start()
    {       
        player = GameObject.FindGameObjectWithTag("Player");
        _spawnPoint = FindObjectOfType<CheckPoint>();
        _respawn = FindObjectOfType<CheckPoint>();        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player" && _respawn._checkpoint == true)
        {
            Rigidbody _rb = other.GetComponent<Rigidbody>();
            _rb.velocity = _rb.velocity * 0f;
            player.transform.position = _spawnPoint.SpawnPoint.position;            
        }
        else if (other.gameObject.tag == "Player")
        {
            ReloadLevel();
        }
    }

    public void ReloadLevel()
    {
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }
}


