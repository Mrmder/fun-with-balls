﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Timerbackup : MonoBehaviour
{
    public GameObject timerCanvas;
    public Text _levelCompleteMenu;
    public Text timerText;
    public Text highscoreText;
    public string highscoreForLevel;
    public string highscoreUI;
    public float totalTime;    

    private float highScore;  
    private float time;
    private int minutes;    
    private bool finished = false;
    //private bool timerStart = false;
    private bool firstRun = false;

    void Start()
    {       
        timerCanvas.SetActive(true);

        highScore = PlayerPrefs.GetFloat(highscoreForLevel);

        highscoreText.text = "Best time: " + PlayerPrefs.GetString(highscoreUI);

        if (highScore == 0f)
        {
            firstRun = true;
        }
    }   

    void Update()
    {
        if (finished)
            return;
        /*
        if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D))
        {
            timerStart = true;
        }

        if (timerStart == true)
        {
            */
            time += Time.deltaTime;

            timerText.color = Color.green;

            if (time >= 60)
            {
                minutes += 1;
                time = 0;
            }

            timerText.text = minutes.ToString() + ":" + time.ToString("f1");

            if (minutes > 0 || time > highScore && ! firstRun)
            {
                timerText.color = Color.red;
            }
               
    }

    public void Finish()
    {
        finished = true;        

        totalTime = (minutes * 60) + time;

        //highScore hämtar playerprefs.getfloat för current highscore. 
        if(highScore > totalTime)
        {
            PlayerPrefs.SetFloat(highscoreForLevel, totalTime); //Sparar highscorevärdet för leveln via editor input.
            highscoreText.text = minutes.ToString() + ":" + time.ToString("f1"); //Visar tiden för omgången direkt på highscore UI.
            PlayerPrefs.SetString(highscoreUI, minutes.ToString() + ":" + time.ToString("f1")); //Sparar tiden ovanför som en string och hämtar sedan informationen i Start() vid uppstart.
            _levelCompleteMenu.text = "New high score! Your time was: " + totalTime.ToString("f1");
            Debug.Log("new high score");
        }
        //Samma som ovan + check om det är första gången man spelar genom en firstRun bool (boolen kollar om highscore är 0, om den är det är firstRun = true).
        else if(totalTime > highScore && firstRun == true)
        {
            PlayerPrefs.SetFloat(highscoreForLevel, totalTime);
            highscoreText.text = minutes.ToString() + ":" + time.ToString("f1");
            PlayerPrefs.SetString(highscoreUI, minutes.ToString() + ":" + time.ToString("f1"));
            firstRun = false;
            _levelCompleteMenu.text = "New high score! Your time was: " + totalTime.ToString("f1");
            Debug.Log("first run");
        }
        else if(totalTime == highScore)
        {
            _levelCompleteMenu.text = "You tied the high score! Your time was: " + totalTime.ToString("f1");
            Debug.Log("share the throne");
        }
        else
        {
            _levelCompleteMenu.text = "But you didn't beat the high score, your time was: " + totalTime.ToString("f1");
            Debug.Log("close but no cigarillo");
        }

        timerCanvas.SetActive(false);        
    }
}
