﻿using UnityEngine;
using System.Collections;

public class CheckPoint : MonoBehaviour
{
    public Transform SpawnPoint;

    public bool _checkpoint = false; 

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Debug.Log("Checkpoint mfkr");
            _checkpoint = true;
            SpawnPoint.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        }
    }
}