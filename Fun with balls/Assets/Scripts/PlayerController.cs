﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public float speed;
    public bool _stopperer = true;

    public Rigidbody rb; 

    private void Start()
    {
        rb = GetComponent<Rigidbody>();        
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene(0);
        }
    }

    private void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        Vector3 movement = new Vector3(moveHorizontal, 0.0f);

        //_stopperer stänger av kontroll vid vinst
        if (_stopperer == true)
        {
            rb.AddForce(movement * speed);

            
            if (Input.GetKey(KeyCode.W) || (Input.GetKey(KeyCode.UpArrow)))
            {
                rb.AddForce(Vector3.forward * speed);
            }

              if (Input.GetKey(KeyCode.S) || (Input.GetKey(KeyCode.DownArrow)))
            {
                rb.velocity = rb.velocity * 0.95f;
            }
            
        }

        else
        {
            rb.velocity = rb.velocity * 0f;
        }
    }  
}
