﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pawnmovement : MonoBehaviour
{
    public Vector3 pointA = new Vector3(0, 0, 0);
    public Vector3 pointB = new Vector3(0, 0, 0);
    public float speed;
    public float pongDelay;

    void Update()
    {
        transform.position = Vector3.Lerp(pointA, pointB, Mathf.PingPong(Time.time * speed, pongDelay));
    }
}
