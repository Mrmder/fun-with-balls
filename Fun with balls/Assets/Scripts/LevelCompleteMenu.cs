﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelCompleteMenu : MonoBehaviour
{    
    public int nextLevel;    
    public int levelToUnlock = 2;
    public string _nextLevel = "level 2";
    
    public void WinLevel()
    {
        Debug.Log("Level Won! Unlocking next level");
        PlayerPrefs.SetInt("levelReached", levelToUnlock);
    }
    
    public void NextLevel()
    {
        SceneManager.LoadScene(nextLevel);
    }    

    public void MainMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void ReloadLevel()
    {
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);        
    }
}
