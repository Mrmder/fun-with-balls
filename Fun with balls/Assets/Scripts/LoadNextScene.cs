﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class LoadNextScene : MonoBehaviour
{
    LevelCompleteMenu _nextLevel;
    public LevelCompleteMenu levelComplete;
    public GameObject StageClearCanvas;
    public GameObject enemies;
    public AudioSource levelMusic;
    public AudioSource winMusic;

    //private PlayerController _pc;     

    private void Start()
    {
        winMusic.enabled = false;
        levelMusic.enabled = true;
        _nextLevel = FindObjectOfType<LevelCompleteMenu>();
       // _pc = FindObjectOfType<PlayerController>();
        StageClearCanvas.SetActive(false);
    }    

    private void OnTriggerEnter(Collider other)
    {
        //Stänger av enemymovement, stannar spelaren, drar upp "wincanvas", kallar på finishmetoden i timerscriptet.
        if (other.gameObject.tag == "Player")
        {
            levelMusic.enabled = false;
            winMusic.enabled = true;
            enemies.SetActive(false);
            //_pc._stopperer = false;
            GameObject.Find("GameManager").SendMessage("Finish");
            if (_nextLevel.nextLevel != 4)
            {
                levelComplete.WinLevel();
                StageClearCanvas.SetActive(true);
            }
            else
            {
                SceneManager.LoadScene(4);
            }
        } 
    }
}
            
    
    

        

      
