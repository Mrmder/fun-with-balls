﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HazardMovementSpook2 : MonoBehaviour
{
    private double hazardMinX;
    private double hazardMaxX;
    private double hazardMinY;
    private double hazardMaxY;
    bool xPosition = true;
    bool yPosition = false;
    public float speed = 0f;
    public float area = 3;

    private void Start()
    {
        hazardMinX = transform.position.x - area;
        hazardMaxX = transform.position.x + area;
        hazardMinY = transform.position.y - area;
        hazardMaxY = transform.position.y + area;

    }

    private void One()
    {
        transform.position += new Vector3(0.1f + speed, 0, 0);
    }

    private void Two()
    {
        transform.position += new Vector3(0, 0.1f + speed, 0);
    }

    private void Three()
    {
        transform.position += new Vector3(-0.1f - speed, 0, 0);
    }

    private void Four()
    {
        transform.position += new Vector3(0, -0.1f - speed, 0);
    }

    void FixedUpdate()
    {
        if (transform.position.x <= hazardMinX)
            xPosition = true;
        if (transform.position.x >= hazardMaxX)
            xPosition = false;
        if (transform.position.y <= hazardMinY)
            yPosition = false;
        if (transform.position.y >= hazardMaxY)
            yPosition = true;



        if (xPosition && !yPosition)
            One();
        else if (!xPosition && !yPosition)
            Two();
        else if (!xPosition && yPosition)
            Three();
        else if (xPosition && yPosition)
            Four();




    }
}
