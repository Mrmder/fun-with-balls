﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shark2 : MonoBehaviour
{
    private double hazardMin;
    private double hazardMax;
    private double hazardMinHeight;
    private double hazardMaxHeight;
    bool height;
    bool xPosition;
    public float speed = 0f;
    public float speedx = 0f;

    private void Start()
    {
        hazardMin = transform.position.x;
        hazardMax = transform.position.x + 40;
        hazardMinHeight = transform.position.y;
        hazardMaxHeight = transform.position.y + 30;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (transform.position.y <= hazardMinHeight)
            height = true;
        else if (transform.position.y >= hazardMaxHeight)
            height = false;

        if (height)
        {
            transform.position += new Vector3(0, 0.01f + speed, 0);
        }
        else if (!height)
        {
            transform.position += new Vector3(0, -0.01f - speed, 0);
        }

        if (transform.position.x <= hazardMin)
            xPosition = true;
        else if (transform.position.x >= hazardMax)
            xPosition = false;

        if (xPosition)
        {
            transform.position += new Vector3(0.01f + speedx, 0, 0);
        }
        else if (!xPosition)
        {
            transform.position += new Vector3(-0.01f - speedx, 0, 0);
        }
    }
}